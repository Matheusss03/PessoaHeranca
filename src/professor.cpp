#include <iostream>
#include "professor.hpp"

Professor::Professor()
{
	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula(0);
	setSalario(0);
	setDisciplina("");

}

Professor::Professor(string nome, string idade, string telefone, int matricula, float salario, string disciplina)
{
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
	setSalario(salario);
	setDisciplina(disciplina);
}

void Professor::setMatricula(int matricula)
{
	this->matricula = matricula;
}

int Professor::getMatricula()
{
	return matricula;
}

void Professor::setSalario(float salario)
{
	this->salario = salario;
}

float Professor::getSalario()
{
	return salario;
}

void Professor::setDisciplina(string disciplina)
{
	this->disciplina = disciplina;
}

string Professor::getDisciplina()
{
	return disciplina;
}
