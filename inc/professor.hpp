#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

class Professor: public Pessoa
{
	private:
		string disciplina;
		float salario;
		int matricula;
	public:
		Professor();
		Professor(string nome, string idade, string telefone, int matricula,float salario, string disciplina);

		void setDisciplina(string disciplina);
		string getDisciplina();
		void setSalario(float salario);
		float getSalario();
		void setMatricula(int matricula);
		int getMatricula();

};

#endif
